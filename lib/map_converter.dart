library map_converter;

abstract class MC {
  static String? getString(dynamic object) {
    String? result = object?.toString();
    return (result == null)
        ? null
        : result.trim().isEmpty
            ? null
            : result.trim();
  }

  static double? getDouble(dynamic object) {
    String? result = object?.toString();
    if (result == null || result.trim().isEmpty) {
      return null;
    }

    return double.tryParse(result);
  }

  static int? getInt(dynamic object) {
    String? result = object?.toString();
    if (result == null || result.trim().isEmpty) {
      return null;
    }

    return int.tryParse(result);
  }

  static bool? getBool(dynamic object) {
    String? result = object?.toString();

    if (result == null || result.trim().isEmpty) {
      return null;
    } else if (result.trim().toLowerCase() == "true") {
      return true;
    } else if (result.trim().toLowerCase() == "false") {
      return false;
    } else {
      return null;
    }
  }

  static T? get<T>(dynamic object) {
    try {
      return object;
    } catch (_) {
      return null;
    }
  }
}
